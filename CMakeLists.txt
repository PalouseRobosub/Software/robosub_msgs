cmake_minimum_required(VERSION 3.5)
project(robosub_msgs)

set(CMAKE_CXX_STANDARD 14)
set(FastRTPS_INCLUDE_DIR /opt/ros/`rosversion -d`/include)
set(FastRTPS_LIBRARY_RELEASE /opt/ros/`rosversion -d`/lib/libfastrtps.so)

#find required components
find_package(rosidl_default_generators REQUIRED)
find_package(geometry_msgs REQUIRED)
find_package(std_msgs REQUIRED)
find_package(builtin_interfaces REQUIRED)


################################################
## Declare ROS messages, services and actions ##
################################################

set(msg_files 
    msg/BatteryDetailed.msg
    msg/Control.msg
    msg/ControlStatus.msg
    msg/DetectionArray.msg
    msg/Detection.msg
    msg/Euler.msg
    msg/Float32Stamped.msg
    msg/Gamepad.msg
    msg/HydrophoneBearing.msg
    msg/HydrophoneDeltas.msg
    msg/HydrophoneStatus.msg
    msg/Joystick.msg
    msg/ObstaclePosArray.msg
    msg/ObstaclePos.msg
    msg/PositionArrayStamped.msg
    msg/Position.msg
    msg/PositionsStamped.msg
    msg/RGBArray.msg
    msg/RGB.msg
    msg/Thruster.msg
    msg/Trax.msg
    msg/VectorSpherical.msg
    msg/VisionPosArray.msg
    msg/VisionPos.msg
)

rosidl_generate_interfaces(${PROJECT_NAME}
  ${msg_files}
  DEPENDENCIES geometry_msgs std_msgs builtin_interfaces
)

ament_export_dependencies(rosidl_default_runtime)

ament_package()

